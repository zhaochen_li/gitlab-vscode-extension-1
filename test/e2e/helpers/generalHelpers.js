import { browser } from '@wdio/globals';
import { v4 as uuidv4 } from 'uuid';

/**
 * Generates a random alphanumeric string from 1 to 32 characters long
 *
 * @param {string|number} length - The length of the random string
 * @returns {String}
 */
const generateRandomString = length => {
  const uuid = uuidv4();
  const randomString = uuid.replace(/-/g, '');

  return randomString.slice(0, Number(length));
};

const waitForEditorTab = async (editorView, filename) => {
  await browser.waitUntil(
    async () => {
      const openTabs = await editorView.getOpenEditorTitles();
      return openTabs.includes(filename);
    },
    {
      timeoutMsg: `Tab for ${filename} did not appear.`,
    },
  );
};

export { generateRandomString, waitForEditorTab };
