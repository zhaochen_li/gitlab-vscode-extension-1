#!/bin/sh

extracted_content=""
in_range=false

# move up to parent directory
cd "$(dirname "$0")/.." || exit
file_path="CHANGELOG.md"

# Read the file line by line
while IFS= read -r line || [ -n "$line" ]; do
  # Check if the line matches the pattern
  # if grep -qE '^(## \[|# \[)' <<< "$line"; then
  if echo "$line" | grep -qE '^(## \[|# \[)'; then
    if $in_range; then
      break
    else
      in_range=true
    fi
  fi
  
  # Save the line if in range
  if $in_range; then
    extracted_content="${extracted_content}$line
"
  fi
done < "$file_path"

# Output the extracted content
echo "$extracted_content"
