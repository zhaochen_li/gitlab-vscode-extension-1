import { BaseLanguageClient } from 'vscode-languageclient';
import { CodeSuggestionsLSState } from '@gitlab-org/gitlab-lsp';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { LanguageServerPolicy, UNSUPPORTED_LANGUAGE } from './language_server_policy';

describe('LanguageServerPolicy', () => {
  let policy: LanguageServerPolicy;
  const mockOnNotification = jest.fn();
  const client = createFakePartial<BaseLanguageClient>({
    onNotification: mockOnNotification,
    sendNotification: jest.fn(),
  });
  let sendNotification: (params: CodeSuggestionsLSState) => Promise<void>;
  const engageListener = jest.fn();

  beforeEach(async () => {
    policy = new LanguageServerPolicy(client);
    mockOnNotification.mockImplementation((_, _callback) => {
      sendNotification = _callback;
    });
    await policy.init();

    policy.onEngagedChange(engageListener);
  });

  it('is engaged when LS notifies about the code suggestions state change with NON-empty state', async () => {
    await sendNotification(UNSUPPORTED_LANGUAGE);

    expect(policy.engaged).toBe(true);
    expect(engageListener).toHaveBeenCalledWith(true);
  });

  it('is NOT engaged when LS notifies about the code suggestions state with an empty state', async () => {
    await sendNotification(undefined);

    expect(policy.engaged).toBe(false);
    expect(engageListener).toHaveBeenCalledWith(false);
  });
});
