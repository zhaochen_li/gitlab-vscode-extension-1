import * as vscode from 'vscode';
import { CONFIG_NAMESPACE } from '../constants';

export const createFakeWorkspaceConfiguration = (
  config: Record<string, unknown>,
): vscode.WorkspaceConfiguration => config as unknown as vscode.WorkspaceConfiguration;

export const setFakeWorkspaceConfiguration = (config: Record<string, unknown>) => {
  const configuration = createFakeWorkspaceConfiguration(config);

  jest.mocked(vscode.workspace.getConfiguration).mockImplementation(section => {
    if (section === CONFIG_NAMESPACE) {
      return configuration;
    }

    return createFakeWorkspaceConfiguration({});
  });
};

type VoidFunction = () => void;

export const createConfigurationChangeTrigger = () => {
  const settingsRefreshTriggers: VoidFunction[] = [];

  jest.mocked(vscode.workspace.onDidChangeConfiguration).mockImplementation(listener => {
    settingsRefreshTriggers.push(() => listener({ affectsConfiguration: () => true }));

    return { dispose: () => {} };
  });

  return async () => {
    if (!settingsRefreshTriggers.length) {
      throw new Error('no setting change listeners were registered');
    }

    await Promise.all(
      settingsRefreshTriggers.map(async trigger => {
        await trigger();
      }),
    );
  };
};

export const createActiveTextEditorChangeTrigger = () => {
  let triggerTextEditorChange: ((te: vscode.TextEditor) => Promise<void> | void) | undefined;

  jest.mocked(vscode.window.onDidChangeActiveTextEditor).mockImplementation(listener => {
    triggerTextEditorChange = async te => listener(te);
    return { dispose: () => {} };
  });
  return async (te: vscode.TextEditor) => {
    if (!triggerTextEditorChange) {
      throw new Error('no active editor change listeners were registered');
    }
    await triggerTextEditorChange(te);
  };
};

export const createTelemetryChangeTrigger = () => {
  let triggerTelemetryChange: ((enabled: boolean) => Promise<void> | void) | undefined;

  jest.mocked(vscode.env.onDidChangeTelemetryEnabled).mockImplementation(listener => {
    triggerTelemetryChange = async enabled => listener(enabled);
    return {
      dispose: () => {},
    };
  });
  return async (enabled: boolean) => {
    if (!triggerTelemetryChange) {
      throw new Error('no telemetry change listeners were registered');
    }
    await triggerTelemetryChange(enabled);
  };
};
