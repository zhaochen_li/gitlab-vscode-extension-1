import * as vscode from 'vscode';
import { GitLabTelemetryEnvironmentDesktop } from './gitlab_telemetry_environment_desktop';
import { createTelemetryChangeTrigger } from '../../common/test_utils/vscode_fakes';

describe('GitLabTelemetryEnvironmentDesktop', () => {
  let telemetryEnvironmentDesktop: GitLabTelemetryEnvironmentDesktop;
  const triggerTelemetrySettingChange = createTelemetryChangeTrigger();
  describe('isTelemetryEnabled', () => {
    it.each`
      telemetryStatus
      ${true}
      ${false}
    `(
      'returns $telemetryStatus when vscode.env.isTelemetryEnabled is $telemetryStatus',
      ({ telemetryStatus }) => {
        Object.assign(vscode.env, { isTelemetryEnabled: telemetryStatus });
        telemetryEnvironmentDesktop = new GitLabTelemetryEnvironmentDesktop();

        expect(telemetryEnvironmentDesktop.isTelemetryEnabled()).toBe(telemetryStatus);
      },
    );
  });

  it('emits telemetry state change on VSCode setting change', async () => {
    const isTelemetryEnabled = true;

    const telemetryChangeListener = jest.fn();

    telemetryEnvironmentDesktop.onDidChangeTelemetryEnabled(isEnabled =>
      telemetryChangeListener(isEnabled),
    );
    await triggerTelemetrySettingChange(isTelemetryEnabled);
    expect(telemetryChangeListener).toHaveBeenCalledWith(isTelemetryEnabled);
  });
});
